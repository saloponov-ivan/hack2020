from sqlalchemy import sql
from sqlalchemy.orm import query
from flask_sqlalchemy import SQLAlchemy


class DlainQuery(query.Query):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def filter_by(self, **kwargs):
        new_kwargs = kwargs.copy()
        new_kwargs.setdefault("deleted", None)
        clauses = [query._entity_descriptor(self._joinpoint_zero(), key) == value
                   for key, value in new_kwargs.items()]
        return self.filter(sql.and_(*clauses))

    def filter_by_ignore_deleted(self, **kwargs):
        return super().filter_by(**kwargs)


db = SQLAlchemy(query_class=DlainQuery)