class ConfigError(Exception):
    pass


class ConfigPathInvalid(ConfigError):
    """Config path is invalid."""
    pass


class ConfigValidationError(ConfigError):
    """Config validation error."""
    pass


class ConfigNotLoadedError(ConfigError):
    """Config not loaded error"""
    pass