import os
import sys
import yaml

from schematics.exceptions import DataError

from schematics.models import Model
from schematics.types import StringType, IntType, ModelType, BooleanType

import service.config.exceptions as config_exceptions


RAW_CONFIG = None


class DBConfig(Model):
    name = StringType(required=True)
    user = StringType(required=True)
    host = StringType(required=True)
    port = IntType(default=5432)
    pool_size = IntType(default=20)
    max_overflow = IntType(default=25)
    # json_sort_keys = BooleanType()


class ServerConfig(Model):
    port = IntType(required=True)
    host = StringType(required=True)
    debug = BooleanType(default=False)


# class MinioConfig(Model):
#     host = StringType(required=True)
#     access_key_env = StringType(default="MINIO_ACCESS_KEY")
#     secret_key_env = StringType(default="MINIO_SECRET_KEY")
#     person_bucket = StringType(default="persons")
#     locations_bucket = StringType(default="locations")



class AppConfig(Model):
    server = ModelType(ServerConfig, required=True)
    database = ModelType(DBConfig, required=True)

def load(path, validate=True):
    if not os.path.isfile(path):
        raise config_exceptions.ConfigPathInvalid("Config path {} is invalid".format(path))

    with open(path, 'r') as stream:
        try:
            yaml_conf = yaml.safe_load(stream)
        except yaml.YAMLError:
            raise config_exceptions.ConfigValidationError("Could not load config from file {}".format(path))

        app_conf = AppConfig(yaml_conf)
        try:
            if validate:
                app_conf.validate()

            global RAW_CONFIG
            RAW_CONFIG = app_conf
            return app_conf
        except DataError as e:
            print(e, file=sys.stderr)
            exit(1)


def get_config() -> AppConfig:
    return RAW_CONFIG
