import enum
import os


from sqlalchemy import *
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm.events import event

from service.query import db

#
# camera_group_memberships = db.Table('camera_group_memberships',
#     db.Column('group_id', db.Integer, db.ForeignKey('camera_groups.id'), primary_key=True),
#     db.Column('member_id', db.Integer, db.ForeignKey('cameras.id'), primary_key=True)
# )



class Odh(db.Model):
    __tablename__ = "odh"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    distance = db.Column(db.Float)
    layout_length = db.Column(db.Float)
    geometry = db.Column(JSON(none_as_null=True))
    json = db.Column(JSON(none_as_null=True))
    #
    # __table_args__ = (
    #     Index(
    #         'idx_odh_tsv',
    #         # to_tsvector_mail
    #     )
    # )

    def __repr__(self):
        return "<id {}>".format(self.id)


odh_index = DDL("CREATE INDEX idx_odh_ft  ON odh USING GiST(to_tsvector('russian', name));")
event.listen(Odh.__table__, 'after_create', odh_index.execute_if(dialect='postgresql'))


class Title(db.Model):
    __tablename__ = "titles"

    id = db.Column(db.Integer, primary_key=True)
    ext_id = db.Column(db.Integer)

    name = db.Column(db.String)
    border_start = db.Column(db.String)
    border_end = db.Column(db.String)
    region = db.Column(db.String)
    reason = db.Column(db.String)
    program = db.Column(db.String)
    category = db.Column(db.String)
    carriageway_square = db.Column(db.Float, nullable=True)
    border_square = db.Column(db.Float, nullable=True)
    roadside_square = db.Column(db.Float, nullable=True)
    total_square = db.Column(db.Float, nullable=True)
    deleted = db.Column(db.TIMESTAMP, nullable=True)

    def __repr__(self):
        return "<id {}, name {}>".format(self.id, self.name)