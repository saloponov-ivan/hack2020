import flask
from flask_restplus import Resource

from service.api.api import api
from service.api.validation import validate
from schematics import Model
from schematics.types import BaseType
from service.logger import log
from service.models import Title
from service.daemon import db
import io
import openpyxl

ns = api.namespace("titles")

class UploadTitleRequest(Model):
    titles = BaseType()



@ns.route('')
class Titles(Resource):
    @ns.expect()
    def get(self):
        pass

    @api.response(200, "File uploaded")
    def post(self):
        request = validate(flask.request.files, UploadTitleRequest)
        if request.titles is None:
            flask.abort(400, {"message": "No titles found"})

        xlsx = io.BytesIO(request.titles.read())
        wb = openpyxl.load_workbook(xlsx)
        for sheet in wb.worksheets:
            for cells in sheet.iter_rows():
                log.debug([cell.value for cell in cells])
                id = self.safe_cast(cells[0], int)
                if id is not None:
                    db.session.add(Title(ext_id=id,
                                         name=cells[1],
                                         border_start=cells[2],
                                         border_end=cells[3],
                                         region=cells[4],
                                         reason=cells[5],
                                         program=cells[6],
                                         category=cells[7],
                                         carriageway_square=self.safe_cast(cells[8], float),
                                         border_square=self.safe_cast(cells[9], float),
                                         ))
            db.session.commit()

        # id = db.Column(db.Integer, primary_key=True)
        # ext_id = db.Column(db.Integer)
        #
        # name = db.Column(db.String)
        # border_start = db.Column(db.String)
        # border_end = db.Column(db.String)
        # region = db.Column(db.String)
        # reason = db.Column(db.String)
        # program = db.Column(db.String)
        # category = db.Column(db.String)
        # carriageway_square = db.Column(db.Float, nullable=True)
        # border_square = db.Column(db.Float, nullable=True)
        # roadside_square = db.Column(db.Float, nullable=True)
        # total_square = db.Column(db.Float, nullable=True)
        # deleted = db.Column(db.TIMESTAMP, nullable=True)
        # try:
        #     url = app.minio.save_title_file(request.titles)
        # except FileParsingException as e:
        #     log.error(e)
        #     flask.abort(400, {"message": "Failed to extract file from request"})
        #     return

        # TODO: save file into minio and parse
        return flask.json({"test": "titles"})

    def safe_cast(val, to_type, default=None):
        try:
            return to_type(val)
        except (ValueError, TypeError):
            return default
