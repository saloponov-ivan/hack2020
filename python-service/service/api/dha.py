import flask
from flask_restplus import Resource

from service.api.api import api
from service.api.validation import validate
from schematics import Model
from schematics.types import BaseType, StringType, TimestampType
from service.logger import log
from datetime import datetime

ns = api.namespace("dha")


class SearchNearestObjects(Model):
    parse = StringType()


class UploadOdhFile(Model):
    file = BaseType()
    timestamp = TimestampType(required=False)


@ns.route('')
class Titles(Resource):
    @ns.expect()
    def get(self):
        request = validate(flask.request.args, SearchNearestObjects)

        return flask.json({"ok": "ok"})

    @api.response(200, "File uploaded")
    def post(self):
        request = validate(flask.request.files, UploadOdhFile)
        if request.titles is None:
            flask.abort(400, {"message": "No file found"})

        if request.timestamp is None:
            request.timestamp = datetime.now()
            print(request.timestamp)

        # TODO: save into minio
        # try:
        #     url = app.minio.save_title_file(request.titles)
        # except FileParsingException as e:
        #     log.error(e)
        #     flask.abort(400, {"message": "Failed to extract file from request"})
        #     return

        # TODO: save file into minio and parse
        return flask.json({"": "titles"})