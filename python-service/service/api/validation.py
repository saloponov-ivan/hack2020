import flask

from schematics.exceptions import DataError, ValidationError
from schematics.types.base import TypeMeta


def validate(data, model: TypeMeta):
    try:
        result = model(data)
        result.validate()
        return result
    except (DataError, ValidationError) as e:
        flask.abort(400, {"message": e.to_primitive()})
