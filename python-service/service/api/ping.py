import flask
from flask_restplus import Resource

from service.api.api import api


ns = api.namespace("ping")


@ns.route('')
class Ping(Resource):
    def get(self):
        return flask.jsonify({"ping": "pong"})
