import json
from service.daemon import app

@app.route("/test")
def test():
    return Response("test", mimetype='application/json')

# from service.api import http_errors
from service.api import ping, titles, dha