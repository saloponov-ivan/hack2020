import argparse
import os

from flask import Flask, Blueprint
from flask_cors import CORS
from flask_bcrypt import Bcrypt
from flask_migrate import Migrate

from service.query import db
import service.constants as constants
from service.config import config
from service import app



from service.api.api import api


def parse_args():
    parser = argparse.ArgumentParser(description="DLAIN Team. Hack 2020 service.")
    parser.add_argument("--port", default=4444, help="port to listen on (hack app)")
    # parser.add_argument("--analytics-port", default=4445, help="port to listen on (analytics service)")
    parser.add_argument("--config", default="/python-service/configs/config.yaml",
                        help="file to get configuration from")
    return parser.parse_args()


def main():
    args = parse_args()
    print(args)
    app_config = config.load(args.config)

    app.config.update(**{constants.APP_CONFIG: app_config})

    database_password = os.getenv("POSTGRES_PASSWORD", "")
    database_host = os.getenv("POSTGRES_HOST", app.config[constants.APP_CONFIG].database.host)
    database_user = os.getenv("POSTGRES_USER", app.config[constants.APP_CONFIG].database.user)
    database_db = os.getenv("POSTGRES_DB", app.config[constants.APP_CONFIG].database.name)
    database_port = os.getenv("POSTGRES_PORT", app.config[constants.APP_CONFIG].database.port)

    database_uri = 'postgresql://{user}:{password}@{host}:{port}/{name}'.format(
        user=database_user,
        password=database_password,
        host=database_host,
        port=database_port,
        name=database_db,
    )

    app.config.update(SQLALCHEMY_DATABASE_URI=database_uri)
    app.config.update(SQLALCHEMY_TRACK_MODIFICATIONS=True)
    app.config.update(SQLALCHEMY_POOL_SIZE=app.config[constants.APP_CONFIG].database.pool_size)
    app.config.update(SQLALCHEMY_MAX_OVERFLOW=app.config[constants.APP_CONFIG].database.max_overflow)
    app.url_map.strict_slashes = False

    CORS(app)

    app.register_blueprint(Blueprint('default', __name__, url_prefix=''))

    api.init_app(app)
    db.init_app(app)
    Migrate(app, db)

    app.run(
        host=app.config[constants.APP_CONFIG].server.host,
        port=app.config[constants.APP_CONFIG].server.port,
        debug=app.config[constants.APP_CONFIG].server.debug, threaded=True
    )

# db.init_app(app)
    # Migrate(app, db)

    # app.jwt = dlain_jwt.DlainJWT(app_config.jwt)
    #
    # app.minio = dlain_minio.DlainMinio(app_config.minio)

    # app.run(
    #     host=app.config[constants.APP_CONFIG].server.host,
    #     port=app.config[constants.APP_CONFIG].server.port,
    #     debug=app.config[constants.APP_CONFIG].server.debug, threaded=True
    # )


if __name__ == "__main__":
    main()
