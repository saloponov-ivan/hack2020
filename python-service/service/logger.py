import logging


# FIXME: Explore how to setup logging in docker applications
log_format = "%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s"
logging.basicConfig(level=logging.DEBUG, datefmt="%Y-%m-%d %H:%M:%S", format=log_format)
log = logging.getLogger()
