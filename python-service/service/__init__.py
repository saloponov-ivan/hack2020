from flask import Flask
from flask_bcrypt import Bcrypt

app = Flask(__name__)
app_bcrypt = Bcrypt(app)