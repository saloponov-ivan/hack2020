from setuptools import setup, find_packages


if __name__ == "__main__":
    setup(
        name="hack2020",
        version="0.1",
        description="Hack 2020 service",
        author_email="saloponov.ivan@gmail.com",
        packages=find_packages(),
        # FIXME: add install_requires section
        scripts=[
            "bin/daemon",
        ],
    )
