#!/usr/bin/env python3

import argparse
import os
import io
import json
from flask import Flask
import service.config.config as config
from service.models import Odh
from service.query import db



def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--drop", action="store_true", dest="drop",
        help="Drop all tables before initializing"
    )
    parser.add_argument("--config", default="/python-service/configs/config.yaml", help="file to get configuration from")
    parser.add_argument("--odxfile", default="/python-service/dataset/odh20200928.json", help="file with map objects")
    parser.add_argument("--titles", default="/python-service/dataset/odh20200928.json", help="file with map objects")
    return parser.parse_args()


def parse_odx(odx_file):
    odx_file = open(odx_file, 'r')
    count = 0

    while True:
        count += 1

        # Get next line from file
        line = odx_file.readline()

        # if line is empty
        # end of file is reached
        if not line:
            break
        print("Line{}: {}".format(count, line.strip()))
        odh_json_object = json.loads(line)
        odh = Odh(id=count,
            name=odh_json_object.get('name'),
            distance=odh_json_object.get('distance'),
            layout_length=odh_json_object.get('layout_length'),
            geometry=odh_json_object.get('geometry'),
            json=line)
        db.session.add(odh)
    odx_file.close()
    db.session.commit()


def main():
    args = parse_args()
    conf = config.load(args.config, validate=False)

    database_password = os.getenv("POSTGRES_PASSWORD", "")
    database_uri = 'postgresql://{user}:{password}@{host}:{port}/{name}'.format(
        user=conf.database.user,
        password=database_password,
        host=conf.database.host,
        port=conf.database.port,
        name=conf.database.name,
    )
    app = Flask(__name__)

    app.config.update(SQLALCHEMY_DATABASE_URI=database_uri)
    app.config.update(SQLALCHEMY_TRACK_MODIFICATIONS=False)
    # app.config.update(JSON_SORT_KEYS=conf.database.json_sort_keys)
    app.config.update(SQLALCHEMY_POOL_SIZE=conf.database.pool_size)
    app.config.update(SQLALCHEMY_MAX_OVERFLOW=conf.database.max_overflow)
    app.url_map.strict_slashes = False

    db.init_app(app)

    with app.app_context():
        if args.drop:
            db.drop_all()

        db.create_all()

        parse_odx(args.odxfile)
        parse_titles(args.titles)
        # add_neuronets()
        # add_car_models()
        # add_users()


if __name__ == "__main__":
    main()