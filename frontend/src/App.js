import React from 'react';
import logo from './logo.svg';
import './App.css';
// import './FileUpload'

function App() {
  return (
    <div className="App">
        <header className="page-header">
        </header>
        <img src={logo} className="app-logo" alt="logo" />
            <main>
                <nav className="page-nav">
                    <ul>
                        <li data-nav="adresses">Адреса</li>
                        <li data-nav="map">Карта</li>
                        <li data-nav="reports">Отчеты</li>
                        <li data-nav="upload">Загрузить</li>
                    </ul>
                </nav>

                <section className="adresses">

                </section>

                <section className="map hidden">

                </section>

                <section className="reports hidden">

                </section>

                <section className="upload hidden">
                    <form method="post">
                        <span>Перетащите файлы сюда</span>
                        <label className="button upload-btn">
                            Выберите файлы
                            <input type="file" name="upload-file"></input>
                        </label>
                    </form>
                </section>

            </main>
            <footer className="page-footer">
                <p className="corp-name">DLAIN Corporation</p>
                <div className="right-col">
                    <a href="http://mos.ru">mos.ru</a>
                    <div className="socials">
                        <a href="http://facebook.com" className="social social-fb"></a>
                        <a href="http://twitter.com" className="social social-tw"></a>
                    </div>
                </div>
            </footer>
    </div>
  );
}

export default App;
